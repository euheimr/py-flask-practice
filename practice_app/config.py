
class BaseConfig(object):
    """Base Config Class"""
    SECRET_KEY = 'DWG$GFGS52grgs5324dFSG$2ds&%#gffER561f23564afe4w56af4eFW$G1rtsgffW$GaF'
    DEBUG = True
    TESTING = False


class ProductionConfig(BaseConfig):
    """Production Configuration"""
    DEBUG = False
    SECRET_KEY = open('/key/key').read()


class StagingConfig(BaseConfig):
    """Staging Configuration"""
    DEBUG = True


class DevelopmentConfig(BaseConfig):
    """Development Environment Configuration"""
    DEBUG = True
    TESTING = True

    SECRET_KEY = 'GREffq342R$#@AFg43FQWG423fgTGY%#Qfew564941dd45wegG$#S!$fqwfgw$%#aG$G%H'
